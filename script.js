/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?

Отже, основна відмінність між ними полягає в тому, що setTimeout виконує код лише один раз з певною затримкою, тоді як setInterval виконує код з певною затримкою регулярно, вказану між викликами.


2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
Щоб припинити виконання функції, запланованої для виклику з використанням методів setTimeout та setInterval, необхідно використовувати метод clearTimeout або clearInterval, відповідно.


Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/ 
const btn = document.querySelector(".main-button");

const timeout = document.querySelector(".timeout")

btn.addEventListener("click", () => {
    const div = document.querySelector(".container")
    div.innerText = "Hello";
    btn.disabled = true;
    setTimeout(() => {
        div.innerText = "Операція виконана успішно!"; 
        btn.disabled = false;
    }, 3000)
});

// 2
const countdownElement = document.querySelector("#countdown");
console.log(countdownElement);
    let count = 10;
    countdownElement.innerText = count;

    const countdownInterval = setInterval(() => {
        count--;
            if (count > 0) {
                countdownElement.innerText = count;
            } else {
        clearInterval(countdownInterval);
                countdownElement.innerText = "Зворотній відлік завершено";
            }
        }, 1000); 
